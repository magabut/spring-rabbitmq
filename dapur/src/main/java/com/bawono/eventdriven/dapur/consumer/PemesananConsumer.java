package com.bawono.eventdriven.dapur.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.bawono.eventdriven.dapur.dto.DetailPemesanan;

@Service
public class PemesananConsumer {
    private Logger LOGGER = LoggerFactory.getLogger(PemesananConsumer.class);
    @RabbitListener(queues="${rabbitmq.queue.pemesanan}")
    public void consume(DetailPemesanan detailPemesanan){
        LOGGER.info(String.format("Detail pemesanan diterima => %s", detailPemesanan.toString()));

        //Langkah berikutnya kita bisa menyimpanya di database ...
    }
}
