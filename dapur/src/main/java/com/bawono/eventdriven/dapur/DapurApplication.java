package com.bawono.eventdriven.dapur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DapurApplication {

	public static void main(String[] args) {
		SpringApplication.run(DapurApplication.class, args);
	}

}
