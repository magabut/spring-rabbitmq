package com.bawono.eventdriven.pemesanan.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {
    //variable nama pemesanan queue didefinisikan di application.properties
    @Value("${rabbitmq.queue.pemesanan}")
    private String pemesananQueue;
     //variable nama pelayan queue didefinisikan di application.properties
    @Value("${rabbitmq.queue.pelayan}")
    private String pelayanQueue;
    //variable nama pemesanan exchange didefinisikan di application.properties
    @Value("${rabbitmq.exchange.pemesanan}")
    private String pemesananExchange;
    //variable nama pemesanan exchange didefinisikan di application.properties
    @Value("${rabbitmq.routing.key.pemesanan}")
    private String pemesananRoutingKey;
    //variable nama pemesanan exchange didefinisikan di application.properties
    @Value("${rabbitmq.routing.key.pelayan}")
    private String pelayanRoutingKey;
    //mendefinisikan bean pemesanan queue
    @Bean
    public Queue pemesananQueue(){
        return new Queue(pemesananQueue);
    }
    //mendefinisikan bean pemesanan queue
    @Bean
    public Queue pelayanQueue(){
        return new Queue(pelayanQueue);
    }
    //mendefinisikan bean pemesanan exchange
    @Bean
    public TopicExchange pemesananExchange(){
        return new TopicExchange(pemesananExchange);
    }
    //mendefinisikan bean untuk binding antara exchange dan queue menggunakan routing key
    @Bean
    public Binding binding(){
        return BindingBuilder.bind(pemesananQueue())
        .to(pemesananExchange())
        .with(pemesananRoutingKey);
    }
    //mendefinisikan bean untuk binding pelayan antara exchange dan queue menggunakan routing key
    @Bean
    public Binding pelayanBinding(){
        return BindingBuilder.bind(pelayanQueue())
        .to(pemesananExchange())
        .with(pelayanRoutingKey);
    }
        //message converter untuk konfigurasi java ke json maupun sebaliknya
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }    
    //konfigurasi RabbitTemplate
    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
}
