package com.bawono.eventdriven.pemesanan.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pemesanan {
    private String idPemesanan;
    private String nama;
    private int jumlah;
    private int totalHarga;
}
