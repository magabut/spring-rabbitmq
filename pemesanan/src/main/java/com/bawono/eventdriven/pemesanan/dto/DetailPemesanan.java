package com.bawono.eventdriven.pemesanan.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailPemesanan {
    private String status;
    private String keterangan;
    private Pemesanan pemesanan;
}
