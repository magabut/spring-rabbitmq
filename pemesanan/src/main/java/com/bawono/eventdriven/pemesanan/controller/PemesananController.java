package com.bawono.eventdriven.pemesanan.controller;

import java.util.UUID;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bawono.eventdriven.pemesanan.dto.DetailPemesanan;
import com.bawono.eventdriven.pemesanan.dto.Pemesanan;
import com.bawono.eventdriven.pemesanan.publisher.PemesananProducer;

@RestController
@RequestMapping("/api/pemesanan/v1")
public class PemesananController {
    PemesananProducer pemesananProducer;


    public PemesananController(PemesananProducer pemesananProducer) {
        this.pemesananProducer = pemesananProducer;
    }


    @PostMapping("/pemesanan")
    public String kirimPemesanan(@RequestBody Pemesanan pemesanan){
      pemesanan.setIdPemesanan(UUID.randomUUID().toString());
      DetailPemesanan detailPemesanan = new DetailPemesanan();
      detailPemesanan.setStatus("Pesanan Baru");
      detailPemesanan.setKeterangan("Pesanan baru diterima");
      detailPemesanan.setPemesanan(pemesanan);
      pemesananProducer.sendMessage(detailPemesanan);
       return "Pesanan dikirimkan ke RabbitMQ";
    }
}
