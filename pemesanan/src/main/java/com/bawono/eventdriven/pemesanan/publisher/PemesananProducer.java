package com.bawono.eventdriven.pemesanan.publisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bawono.eventdriven.pemesanan.dto.DetailPemesanan;

@Service
public class PemesananProducer {
    private Logger LOGGER = LoggerFactory.getLogger(PemesananProducer.class);

    // variable nama pemesanan exchange didefinisikan di application.properties
    @Value("${rabbitmq.exchange.pemesanan}")
    private String pemesananExchange;
    // variable nama pemesanan exchange didefinisikan di application.properties
    @Value("${rabbitmq.routing.key.pemesanan}")
    private String pemesananRoutingKey;
    // variable nama pelayan exchange didefinisikan di application.properties
    @Value("${rabbitmq.routing.key.pelayan}")
    private String pelayanRoutingKey;
    private RabbitTemplate rabbitTemplate;

    public PemesananProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(DetailPemesanan detailPemesanan) {
        LOGGER.info(String.format("Order event send to RabbitMQ => %s", detailPemesanan.toString()));
        // kirim pemesanan ke pemesanan queue
        rabbitTemplate.convertAndSend(pemesananExchange, pemesananRoutingKey, detailPemesanan);
        // kirim pemesanan ke email queue
        rabbitTemplate.convertAndSend(pemesananExchange, pelayanRoutingKey, detailPemesanan);
    }

}
