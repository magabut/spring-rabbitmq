package com.bawono.eventdriven.pelayan.consumer;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.bawono.eventdriven.pelayan.dto.DetailPemesanan;

@Service
public class PelayanConsumer {
    private Logger LOGGER = LoggerFactory.getLogger(PelayanConsumer.class);
    @RabbitListener(queues="${rabbitmq.queue.pelayan}")
    public void consume(DetailPemesanan detailPemesanan){
        LOGGER.info(String.format("Detail pemesanan diterima => %s", detailPemesanan.toString()));
    }

}
