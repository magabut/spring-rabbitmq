package com.bawono.eventdriven.pelayan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PelayanApplication {

	public static void main(String[] args) {
		SpringApplication.run(PelayanApplication.class, args);
	}

}
