package com.bawono.eventdriven.pelayan.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailPemesanan {
    private String status;
    private String keterangan;
    private Pemesanan pemesanan;
}
